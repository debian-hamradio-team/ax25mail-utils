/*
   axgetmail
   (c) 1999 by Radek Burget OK2JBG

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version
   2 of the license, or (at your option) any later version.

   conbbs: connect the BBS
*/

#ifndef CONBBS_H
#define CONBBS_H

#include <stdio.h>

FILE *conbbs(char *_mycall, char *_port, char *_dest); /* connect the BBS */
void disc_bbs(FILE *bbs_stream);                       /* disconnect BBS */

#endif

