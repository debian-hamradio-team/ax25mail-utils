/*
   axgetmsg
   (c) 1999 by Radek Burget OK2JBG

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version
   2 of the license, or (at your option) any later version.

   getmsg : read and save the message
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include "common.h"
#include "conbbs.h"
#include "readmsg.h"
#include "auth.h"
#include "getmsg.h"

#ifndef VERSION
#define VERSION "0.09"
#endif

/*Uncomment this for adding subject to the message body*/
/*#define ADD_SUBJECT*/

FILE *bbs_stream = NULL;
static char dest_dir[256];         /* destination directory */
static char prg_name[256];         /* program name (for tag) */
static char curr_call[20];         /* currently used callsign */

/* save decompressed message (and convert to U*ix text) */
int save_msg(int num, void *data, unsigned long length, char *subj)
{
  char fname[256];
  FILE *f;

  sprintf(fname, "%s/%i", dest_dir, num);

  message(MSG_INFO, "Saving message to file %s\n", fname);

  f = fopen(fname, "w");
  if (f != NULL)
  {
    unsigned long i;
    unsigned char *p = (unsigned char *)data;

    #ifdef ADD_SUBJECT
    fprintf(f, "Subject: %s\n", subj);
    #endif

    for (i=0; i<length; i++, p++)
      if (*p != '\r') putc(*p, f);
    fclose(f);
  }
  else
  {
    message(MSG_ERROR, "Cannot write to file %s\n", fname);
    return 0;
  }
  return 1;
}

/* Wait for prompt from BBS. Returns 0 when the connection has been broken */
int wait_prompt(FILE *bbs_stream)
{
  int old_char=0, new_char=0;
  message(MSG_DEBUG, "Waiting for BBS prompt\n");
  do
  {
    old_char = new_char;
    new_char = ffgetc(bbs_stream);
  } while ((old_char != '>' || new_char != '\r') && new_char != EOF);
  return (new_char != EOF);
}

/* send the tag */
void send_tag(FILE *bbs_stream, char *prg_name)
{
  fprintf(bbs_stream, "[%s-%s-$]\r", prg_name, VERSION);
}

/* send the request for message */
void send_request(FILE *bbs_stream, int num)
{
  fprintf(bbs_stream, "F< %i\r", num);
}

/* send the request for killing messages */
void delete_message(FILE *bbs_stream, int num)
{
  fprintf(bbs_stream, "K %i\r", num);
}

/* set program name */
void set_prg_name(char *_prg_name)
{
  strcpy(prg_name, _prg_name);
}

/* read one msg, kill it afterwards if it's private and save it */
/* returns the error code or RET_OK if all OK */
int get_msg(char *mycall, char *port, char *dest, int num, int priv, char *destdir)
{
  char subj[256];
  char *buf;
  int bsize;
  char bbs_call[255];
      
  message(MSG_INFO, "Downloading message %i (%s)\n", num, priv ? "private":"bulletin");
  strcpy(dest_dir, destdir);

  /* connect the BBS if not connected */
  if (bbs_stream == NULL || !compare_call(mycall, curr_call))
  {
    if (bbs_stream != NULL)
    {
      disc_bbs(bbs_stream);
      bbs_stream = NULL;
    }
    message(MSG_INFO, "Connecting BBS");
    bbs_stream = conbbs(mycall, port, dest);
    if (bbs_stream == NULL) return RET_CONN;
    else
    {
      strcpy(curr_call, mycall);
      message(MSG_INFO, " - done.\n");

      message(MSG_INFO, "Running authorization\n");
      strcpy(bbs_call, dest); normalize_call(bbs_call);
      exec_auth_agent(bbs_stream, mycall, bbs_call);

      wait_prompt(bbs_stream);
      message(MSG_INFO, "Sending SID\n");
      send_tag(bbs_stream, prg_name);
      wait_prompt(bbs_stream);
    }
  }
  
  /* read the message */
  message(MSG_INFO, "Sending request\n");
  send_request(bbs_stream, num);
  if (read_msg(bbs_stream, &buf, &bsize, subj))
  {
     /* save the message */
     if (!save_msg(num, buf, bsize, subj)) return RET_WRITE;
     /* kill private messages */
     if (priv && !errmsg)
     {
         wait_prompt(bbs_stream);
         message(MSG_INFO, "Killing the private message\n");
         delete_message(bbs_stream, num);
     }
  }
  else return RET_MSG;
  if (!wait_prompt(bbs_stream)) return RET_MSG;
  return RET_OK;
}

void finish_transfer()
{
  disc_bbs(bbs_stream);
}

