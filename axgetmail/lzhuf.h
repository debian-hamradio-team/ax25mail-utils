/**************************************************************
      lzhuf encoder/decoder class by cz6don, based on:

  ***********************************************************
        lzhuf.c
        written by Haruyasu Yoshizaki 11/20/1988
        some minor changes 4/6/1989
        comments translated by Haruhiko Okumura 4/7/1989
**************************************************************/
/* Back to ansi C rewritten by OK2JBG */

void lzhuf_init(short clu); /* normal clu=1 */
void lzhuf_free();

void lzhuf_encode(unsigned long len, void *data);	// compression
void lzhuf_decode(unsigned long len, void *data, unsigned long plen);	// decompression
unsigned short lzhuf_crc();
void* lzhuf_data();
long lzhuf_length();

