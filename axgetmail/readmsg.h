/*
   axgetmail
   (c) 1999 by Radek Burget OK2JBG

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version
   2 of the license, or (at your option) any later version.

   readmsg: read and decode one message to stdout
*/

#ifndef READMSG_H
#define READMSG_H

#include <stdio.h>

extern int errmsg; /* =1 when error message has been generated instead of dnload */

int read_msg(FILE *bbs_stream, char **_buf, int *_len, char *_subj);

#endif

