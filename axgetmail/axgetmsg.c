/*
   axgetmsg.c
   (c) 1999 by Radek Burget OK2JBG

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version
   2 of the license, or (at your option) any later version.

   axgetmsg -p <port> -c <call> -b <bbs> [-f <listfile>] [-d <dir>]
            [-v] [-D] nr1 [nr2 [...]]
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <getopt.h>
#include "common.h"
#include "getmsg.h"

#ifndef VERSION
#define VERSION "0.09"
#endif

char mycall[20];            /* my callsign */
char bbscall[20];           /* BBS callsign */
char bbspath[256];          /* path to BBS */

char port_name[32];         /* port name */
char dest_dir[256];         /* destination directory */
char listfile[256];         /* name of the list file */

int final_result = RET_OK;  /* result of axgetmsg */

/* check if the parameter was given */
void check_par(char *value, char *msg)
{
  if (strlen(value) == 0)
  {
    message(MSG_ERROR, msg);
    exit(RET_ERR);
  }
}


int main(int argc, char **argv)
{
  int i;

  set_prg_name("axgetmsg");

  if (argc == 1)
  {
    printf("axgetmsg -p <port> -c <call> -b <bbs> [-f <listfile>] [-d <dir>] [-v] [-D] nr1 [nr2 [...]]\n");
    return 1;
  }

  strcpy(port_name, "");
  strcpy(mycall, "");
  strcpy(bbspath, "");
  strcpy(listfile, "");
  strcpy(dest_dir, "");

  while ((i = getopt(argc, argv, "p:c:b:f:d:vD")) != -1)
    switch (i)
    {
      case 'p': strncpy(port_name, optarg, 31);
                break;
      case 'c': strncpy(mycall, optarg, 19);
                normalize_call(mycall);
                break;
      case 'b': strncpy(bbspath, optarg, 255);
                break;
      case 'f': strncpy(listfile, optarg, 255);
                break;
      case 'd': strncpy(dest_dir, optarg, 255);
                break;
      case 'v': msg_level = MSG_INFO;
                break;
      case 'D': msg_level = MSG_DEBUG;
                break;
      case '?':
      default : printf("axgetmsg -p <port> -c <call> -b <bbs> [-f <listfile>] [-d <dir>] [-v] [-D] nr1 [nr2 [...]]\n");
                return RET_ERR;
    }

  check_par(port_name, "Port name missing\n");
  check_par(mycall, "Callsign missing\n");
  check_par(bbspath, "Path to BBS missing\n");
  if (strlen(dest_dir) == 0) strcpy(dest_dir, ".");

  message(MSG_DEBUG, "Params:\n"
                     "BBS : `%s'\n"
                     "Mycall : `%s'\n"
                     "Port : `%s'\n"
                     "Dest dir : `%s'\n",
                     bbspath, mycall, port_name, dest_dir);

  if (strlen(listfile) == 0)
  {
    message(MSG_DEBUG, "Message numbers from command line\n");
    if (optind == argc) //no arguments remaining
    {
      message(MSG_ERROR, "No message numbers given\n");
      return RET_ERR;
    }
    while (optind < argc)
    {
      char *endptr;
      char *pnum;
      int num;
      int priv = 0;
      int result;

      pnum = argv[optind];
      if (toupper(*pnum) == 'P') {priv = 1; pnum++;}

      num = strtol(pnum, &endptr, 10);
      if (*endptr != '\0')
      {
        message(MSG_ERROR, "Invalid message number '%s'\n", argv[optind]);
        return RET_ERR;
      }
      result = get_msg(mycall, port_name, bbspath, num, priv, dest_dir);
      switch (result)
      {
        case RET_ERR: message(MSG_ERROR, "Unable to read message %i\n", num); break;
        case RET_CONN: message(MSG_ERROR, "Cannot connect BBS\n"); break;
        case RET_MSG: message(MSG_ERROR, "Transfer failed for message %i\n", num); break;
        case RET_WRITE: message(MSG_ERROR, "Unable to save message %i\n", num); break;
      }
      optind++;
    }
  }
  else /* listfile used */
  {
    FILE *list;

    message(MSG_DEBUG, "Message numbers from file\n");
    list = fopen(listfile, "r");
    if (list == NULL)
    {
      message(MSG_ERROR, "Cannot open list file '%s'\n", listfile);
      return RET_ERR;
    }
    while (!feof(list))
    {
      char snum[256];
      char *pnum, *endptr;
      int num, n;
      int priv = 0;
      int result;

      n = fscanf(list, "%s", snum);
      if (n <= 0) break;
      pnum = snum;
      if (toupper(*pnum) == 'P') {priv = 1; pnum++;}

      num = strtol(pnum, &endptr, 10);
      if (n != 1 || *endptr != '\0')
      {
        message(MSG_ERROR, "Invalid message number in list file\n");
        return RET_ERR;
      }

      result = get_msg(mycall, port_name, bbspath, num, priv, dest_dir);
      switch (result)
      {
        case RET_ERR: message(MSG_ERROR, "Unable to read message %i\n", num); break;
        case RET_CONN: message(MSG_ERROR, "Cannot connect BBS\n"); break;
        case RET_MSG: message(MSG_ERROR, "Transfer failed for message %i\n", num); break;
        case RET_WRITE: message(MSG_ERROR, "Unable to save message %i\n", num); break;
      }
      if (result != RET_OK) final_result = RET_ERR;
    }
  }

  finish_transfer();
    
  return final_result;
}

