/*
   axgetmsg
   (c) 1999 by Radek Burget OK2JBG

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version
   2 of the license, or (at your option) any later version.

   getmsg : read and save the message
*/
#ifndef GETMSG_H
#define GETMSG_H

#include <stdio.h>

/* set program name (for tag) */
void set_prg_name(char *_prg_name);

/* read one msg, kill it afterwards if it's private and save it */
/* returns the error code or RET_OK if all OK */
int get_msg(char *mycall, char *port, char *dest, int num, int priv, char *destdir);

/* finish download */
void finish_transfer();

#endif

