ax25mail-utils (0.15-1) unstable; urgency=medium

  * Team upload.

  [ Daniele Forsi ]
  * Trim trailing whitespace.
  * Use secure URI in debian/watch.
  * Update standards version to 4.6.0, no changes needed.
  * Use secure URI in debian/control.

  [ Christoph Berg ]
  * New upstream version 0.15.
  * Retire Colin from Uploaders, thanks! (Closes: #990604)

 -- Christoph Berg <myon@debian.org>  Sat, 05 Nov 2022 16:54:32 +0100

ax25mail-utils (0.14-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 0.14.
  * Add patch from upstream to fix gcc-10 issue. (Closes: #957029)
  * Don't install spec file in Debian package.
  * Add debian/gitlab-ci.yml.
  * Remove useless B-D on quilt.
  * DH 13.

 -- Christoph Berg <myon@debian.org>  Wed, 03 Feb 2021 11:57:43 +0100

ax25mail-utils (0.13-1) unstable; urgency=medium

  * New Upstream release.
  * Bump standards version to 3.9.6
  * switch to dh @ to keep autotools up to date.

 -- Colin Tuckley <colint@debian.org>  Thu, 03 Sep 2015 18:58:50 +0100

ax25mail-utils (0.11-7) unstable; urgency=medium

  * Ack the NMU, thanks Joop
  * Bump standards version to 3.9.5
  * Add Hompage to debian/control
  * Update watch file, thanks bartm
  * Fix ax25.h FTBS Closes: #664936
  * Add conf file examples to /usr/share/doc/ax25mail-utils
    Closes: #603282

 -- Colin Tuckley <colint@debian.org>  Sat, 06 Sep 2014 14:36:02 +0100

ax25mail-utils (0.11-6.1) unstable; urgency=low

  * Retiring - remove myself from the uploaders list.

 -- Joop Stakenborg <pa3aba@debian.org>  Tue, 10 Nov 2009 10:14:48 +0000

ax25mail-utils (0.11-6) unstable; urgency=low

  * Fix missing includes by test compiling with gcc/g++-4.3.
    Closes: #456067.
  * Bump standards version.

 -- Joop Stakenborg <pa3aba@debian.org>  Thu, 24 Jan 2008 21:12:09 +0100

ax25mail-utils (0.11-5) unstable; urgency=low

  * Fix FTBFS with gcc-4.3. Closes: #456067. Thanks Martin Michlmayr.

 -- Joop Stakenborg <pa3aba@debian.org>  Sat, 15 Dec 2007 09:27:55 +0100

ax25mail-utils (0.11-4) unstable; urgency=low

  * Fix spelling mistake in package description. Closes: #362746.
  * Let aptitude detect and render nicely the list in the description.
    Closes: #389049.
  * Lintian fixes.
  * Bump standards version to 3.7.2.

 -- Joop Stakenborg <pa3aba@debian.org>  Sun, 02 Dec 2007 12:08:07 +0100

ax25mail-utils (0.11-3) unstable; urgency=low

  * Add an extra space in front of the bullets in the description, so it
    doesn't get mangled. Closes: #304384.

 -- Joop Stakenborg <pa3aba@debian.org>  Wed, 25 May 2005 18:30:20 +0200

ax25mail-utils (0.11-2) unstable; urgency=low

  * New uploader added to the uploaders field.

 -- Joop Stakenborg <pa3aba@debian.org>  Tue, 12 Apr 2005 13:26:05 +0200

ax25mail-utils (0.11-1) unstable; urgency=low

  * Initial Release.

 -- Joop Stakenborg <pa3aba@debian.org>  Sat,  9 Apr 2005 18:20:15 +0200
