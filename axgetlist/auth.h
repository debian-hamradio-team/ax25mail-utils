/*
   axgetmail
   (c) 1999 by Radek Burget OK2JBG

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version
   2 of the license, or (at your option) any later version.

   auth: execute the authorization script
*/

#ifndef AUTH_H
#define AUTH_H

#include <stdio.h>

#define AUTH_AGENT "/var/ax25/auth_agent"

/* run the authorization agent and wait until it exits */
void exec_auth_agent(FILE *bbs_stream, char *mycall, char *bbscall);

#endif

