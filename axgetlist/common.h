/*
   axgetmail
   (c) 1999 by Radek Burget OK2JBG

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version
   2 of the license, or (at your option) any later version.
*/
#ifndef COMMON_H
#define COMMON_H

#include <stdio.h>

/* message priority levels */
#define MSG_DEBUG 0
#define MSG_INFO 1
#define MSG_RESULT 2
#define MSG_ERROR 3 /* the same as MSG_RESULT but written to stderr */

/* program return codes */
#define RET_OK 0
#define RET_ERR 1     /* some other error */
#define RET_CONN 2    /* cannot connect BBS */
#define RET_MSG 3     /* cannot read message */
#define RET_WRITE 4   /* cannot write message */

extern int msg_level; /* the lowest message priority to be displayed */

/* print the message when it's important enough */
void message(int level, const char *fmt, ...);

/* callsign stransformations */
int call_ssid(const char *src);
char *call_call(const char *src);
char *strip_ssid(char *src);
char *normalize_call(char *call);
int compare_call(char *call1, char *call2);

/* wait for the character and read it */
int ffgetc(FILE *f);

/* check if line contains other characters than whitespaces */
int is_empty(char *line);

#endif

