Name:           ax25mail-utils
Version:        0.15
Release:        1%{?dist}
Summary:        AX.25 ham radio mail application
Group:          Applications/Communications
License:        GPLv2+
URL:            http://ax25mail-utils.sourceforge.net/
Source0:        http://sourceforge.net/settings/mirror_choices?projectname=ax25mail&filename=ax25mail-utils/%{version}/ax25mail-utils-%{version}.tar.gz

#BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  autoconf, automake, libtool
BuildRequires:  libax25-devel

%description
This package provides utilities to download a message list or messages from a fbb AX.25 bbs:

 * axgetlist - read the message list from the BBS
 * axgetmail - automatically download messages from the F6FBB BBS
 * axgetmsg - download selected messages from F6FBB BBS
 * home_bbs - find home BBS or force a home BBS for the callsign
 * msgcleanup - delete the messages with their lifetime exceeded
 * ulistd - collect FBB BBS messages list sent via unproto frames
 * update_routes - update the database of BBS and callsigns


%prep
rm -rf $RPM_BUILD_DIR/%{name}-%{version}

%setup -q
autoreconf --install

%build
#LEGACY - the patch requires rebuilding of makefiles and such
#aclocal-1.4; autoconf; automake-1.4

./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var CXXFLAGS="-Wall -funsigned-char"
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

#don't include these twice
rm -rf $RPM_BUILD_ROOT%{_docdir}/%{name}

# Fix the encoding on the doc files to be UTF-8
recode()
{
        iconv -f "$2" -t utf-8 < "$1" > "${1}_"
        mv -f "${1}_" "$1"
}
#recode AUTHORS iso-8859-15


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc BUGS COPYING NEWS README
%{_bindir}/*
#%{_sbindir}/*
%{_localstatedir}/ax25/*
%{_sysconfdir}/ax25/*
#this isn't working
#%dir %{_localstatedir}/ax25/ulistd
%{_mandir}/man?/*
/usr/share/ax25mail-utils/contrib/ax25mail-utils.spec

%changelog
* Mon Feb 1 2021 <axmail@trinnet.net> 0.0.15-1
- New version
* Mon Feb 1 2021 <axmail@trinnet.net> 0.0.14-1
- New version
* Sun Jan 7 2018 <axmail@trinnet.net> 0.0.14-1
- Added missing spec file reference
* Wed Aug 19 2015 <axmail@trinnet.net> 0.0.14-1
- New version
* Sat Jul 11 2015 <axmail@trinnet.net> 0.0.13-1
- fixes for proper autoconf paths and tarball release
* Sat Jun 20 2015 <axmail@trinnet.net> 0.0.12-1
- new release where all code merged, no more patches, proper packagine now works
* Fri Jun 01 2012 <dranch@trinnet.net> 0.0.11-7.2
- Added install-conf to include the example configuration files
* Fri May 25 2012 <dranch@trinnet.net> 0.0.11-7.1
- First RPM version of ax25mail-utils that I can find based on Debian's 0.11-6 patches
- Also fixed ulistd to compile due to the wrong specification of old libs
